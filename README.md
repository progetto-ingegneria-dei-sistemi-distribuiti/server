# Server for OpenSet

### System requirements

- **Java 11** (or higher)
- **Maven** (or use the maven wrapper provided with this repo)
- **RabbitMQ** (if you don't have an active broker or you want to test in local)

Alternatively, you can use **Docker**

## Parameters needed for run the Server
1. **server.port** is the port where the HTTP server must listen (default 8080)
2. **spring.rabbitmq.addresses** to connect to your RabbitMQ broker (default "amqp://guest:guest@localhost/").
3. **rabbitmq.exchange** is the arbitrary name of your RabbitMQ's exchange (default "openset").
4. **openset.topics** is the [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) list of topics that the Server should accept (default **\*** - all topics are accepted).


### Setting up a local istance
First of all, **Clone this repository** or download it as zip.\
If you want to build your own Server, you have to provide the parameters described above to the application. You have three methods to do so:
* Rename the file src/main/resources/application.properties.dist into application.properties and set the fields you need, then run `$ mvn install && mvn spring-boot:run`
* Pass the parameters as command-line argument, like `mvn install && java -jar target/Server*.jar --server.port=80`
* Run it through Docker `docker build -t openset:server . && docker run openset:server`. If you haven't create the application.properties, you can pass each parameter as a Docker enviroment variable like 
    ``` bash
    docker run -d \
     -e spring.rabbitmq.addresses=$BROKER_URL \
     -e rabbitmq.exchange=$EXCHANGE_NAME \
     -e openset.topics=* \
    openset:server
    ```

### RabbitMQ setup
You can easily install RabbitMQ with Docker in two steps:
 1. `docker pull rabbitmq:management`
 2. `docker run -d -p 5672:5672 -p 15672:15672 --name my-rabbit rabbitmq:management`

Alternately, you can install RabbitMQ from your distro's package manager (the following example is for DNF-based distros like Fedora)
 - `dnf install rabbitmq-server`
 - `systemctl start rabbitmq-server.service`
