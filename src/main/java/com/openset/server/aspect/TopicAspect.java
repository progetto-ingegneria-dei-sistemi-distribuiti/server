package com.openset.server.aspect;

import java.util.List;
import java.util.stream.Collectors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class TopicAspect {
	
	@Autowired
	@Qualifier("topics")
	private String[] topics;
	
	@Around("execution(* com.openset.server.web.QueryService.queryDatasources(..))")
	public String checkTopic(ProceedingJoinPoint jp) throws Throwable {
		String argTopics = (String) jp.getArgs()[0];
		String topicsNotSupported = getTopicsNotSupported(argTopics);
		if(!topicsNotSupported.isEmpty())
			return "Topic" + topicsNotSupported + " isn't supported by this Server";
		
		return (String) jp.proceed();
	}

	private String getTopicsNotSupported(String argTopics) {
		List<String> receivedTopics = List.of(argTopics.split("\\|"));
		return receivedTopics.stream().filter(x -> !inTopic(x)).reduce("", (accu,x) -> accu+" "+x);
	}
	
	private boolean inTopic(String receivedTopic) {
		for(String topic : topics) {
			if(topic.equals("*") || topic.toLowerCase().equals(receivedTopic.toLowerCase()))
				return true;
		}
		return false;
	}

}
