package com.openset.server.aspect;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SecureAspect {

	@Value("${openauth.url:localhost}")
	private String openAuthUrl;

	@Value("${openauth.require:false}")
	private boolean requireAuth;

	@Around("@annotation(com.openset.server.annotations.SecureJWT)")
	public ResponseEntity<String> checkAuthorization(ProceedingJoinPoint jp) throws Throwable, Exception {
		if (!requireAuth)
			return (ResponseEntity<String>) jp.proceed();
		else if(jp.getArgs()[2] == null)
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Authentication token not found");
		else
			return checkJWT(jp);
	}

	private ResponseEntity<String> checkJWT(ProceedingJoinPoint jp) throws Throwable {
		String jwt = (String) jp.getArgs()[2];
		HttpClient httpClient = buildClient();
		HttpRequest request = buildRequest(jwt);
		HttpResponse<String> response = null;
		String value = new String();
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (Exception e) {
			value = "Error - service unavailable";
		}

		if (authIsOk(response.statusCode()))
			return (ResponseEntity<String>) jp.proceed();
		else if (response.statusCode() == 404)
			value = "Error - service unavailable";

		return ResponseEntity.status(response.statusCode()).body(value);
	}

	private HttpClient buildClient() {
		HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1).build();
		return httpClient;
	}

	private HttpRequest buildRequest(String jwt) {
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(openAuthUrl))
				.header("Authorization", "Bearer " + jwt).POST(HttpRequest.BodyPublishers.ofString("")).build();
		return request;
	}
	
	private boolean authIsOk(int statusCode) {
		if(statusCode == 200)
			return true;
		else 
			return false;
	}
}
