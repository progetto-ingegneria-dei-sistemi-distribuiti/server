package com.openset.server.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonObject {

	private ObjectMapper mapper;
	private ObjectNode jsonObject;
	
	public JsonObject() {
		mapper = new ObjectMapper();
		jsonObject = mapper.createObjectNode();
	}
	
	protected void put(String key, JsonNode value) {
		jsonObject.set(key, value);
	}

	public void put(String key, JsonArray value) {
		put(key, value.getArrayNode());
	}
	
	@Override
	public String toString() {
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}
}
