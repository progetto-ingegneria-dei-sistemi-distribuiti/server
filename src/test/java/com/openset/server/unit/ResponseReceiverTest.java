package com.openset.server.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.TestPropertySource;

import com.jayway.jsonpath.JsonPath;
import com.openset.server.utils.ResponseReceiver;
import com.rabbitmq.client.Channel;

@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class ResponseReceiverTest {

	@Autowired
	private RabbitTemplate template;
	
	@Autowired
	private ResponseReceiver receiver;

	@Value("#{responseQueue.name}")
	private String responseQueue;

	@Test
	public void getAllMessageWithId_shouldNeverReturnNull() {
		assertNotNull(receiver.getAllMessageWithId("test"));
	}

	@Test
	public void checkGetAllMessageWithIdJsonStructure() throws JSONException {
		String actualStructure = receiver.getAllMessageWithId("test");
		String expectedStructure = "{\"response\":[]}";
		JSONAssert.assertEquals(expectedStructure, actualStructure, JSONCompareMode.LENIENT);
	}
	
	@Test
	public void testReceiver1() {
		MessageProperties prop = new MessageProperties();
		prop.setCorrelationId("test");
		Message m1 = new Message("[{ \"name\":\"John\", \"age\":30, \"car\":\"Ford\" }]".getBytes(), prop);
		Message m2 = new Message("[{ \"name\":\"Will\", \"age\":25, \"car\":\"Alfa\" }]".getBytes(), prop);
		template.convertAndSend(responseQueue, m1, m -> postProcessMessage(m, "test"));
		template.convertAndSend(responseQueue, m2, m -> postProcessMessage(m, "test"));
		String messages = receiver.getAllMessageWithId("test");
		String john = JsonPath.parse(messages).read("$.response[0][0].name").toString();
		assertEquals("John", john);
	}
	
	@Test
	public void testReceiver_withInvalidResponse() {
		MessageProperties prop = new MessageProperties();
		prop.setCorrelationId("test");
		Message msg = new Message("{ \"name\":\"John\", \"age\":30, \"car\":\"Ford\" }".getBytes(), prop);
		template.convertAndSend(responseQueue, msg, m -> postProcessMessage(m, "test"));
		String messages = receiver.getAllMessageWithId("test");
		String response = JsonPath.parse(messages).read("$.response").toString();
		assertEquals("[]", response);
	}
	
	private Message postProcessMessage(Message message, String correlationId) throws AmqpException {
		message.getMessageProperties().setCorrelationId(correlationId);
		return message;
	}

	@TestConfiguration
	@EnableRabbit
	public static class Config {

		@Bean
		@Primary
		public RabbitTemplate template() {
			return new TestRabbitTemplate(connectionFactory());
		}

		@Bean
		public ConnectionFactory connectionFactory() {
			ConnectionFactory factory = mock(ConnectionFactory.class);
			Connection connection = mock(Connection.class);
			Channel channel = mock(Channel.class);
			willReturn(connection).given(factory).createConnection();
			willReturn(channel).given(connection).createChannel(anyBoolean());
			given(channel.isOpen()).willReturn(true);
			return factory;
		}

		@Bean
		public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
			SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
			factory.setConnectionFactory(connectionFactory());
			return factory;
		}

	}

}
