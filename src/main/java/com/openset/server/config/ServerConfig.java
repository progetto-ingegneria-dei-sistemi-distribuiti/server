package com.openset.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.openset.server.aspect.SecureAspect;

@Configuration
public class ServerConfig {

	@Bean(name = "topics")
	public String[] getTopics(@Value("${openset.topics:*}") String csvTopics) {
		String[] topicsArray = csvTopics.split(",");
		for (int i = 0; i < topicsArray.length; i++)
			topicsArray[i] = topicsArray[i].strip();
		return topicsArray;
	}

	@Bean
	public SecureAspect securityAspect() {
		return new SecureAspect();
	}
}
