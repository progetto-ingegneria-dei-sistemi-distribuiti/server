package com.openset.server.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.openset.server.exceptions.JsonParsingException;


public class JsonArray {

	private ObjectMapper mapper;
	private ArrayNode jsonArray;
	
	public JsonArray() {
		mapper = new ObjectMapper();
		jsonArray = mapper.createArrayNode();
	}
	
	public JsonArray(String json) throws JsonParsingException{
		mapper = new ObjectMapper();
		try {
			jsonArray = (ArrayNode) mapper.readTree(json);
		} catch (JsonProcessingException | ClassCastException e ) {
			throw new JsonParsingException(e);
		}
	}
	
	protected void put(JsonNode jsonNode) {
		jsonArray.add(jsonNode);
	}
	
	public void put(JsonArray json) {
		put(json.getArrayNode());
	}
	
	protected ArrayNode getArrayNode() {
		return jsonArray;
	}

}
