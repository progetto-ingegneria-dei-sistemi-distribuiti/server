package com.openset.server.config;

import java.util.Collection;
import java.util.HashSet;

import javax.annotation.Resource;

import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarable;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

	@Value("${rabbitmq.exchange:openset}")
	private String exchange;

	@Resource(name = "topics")
	private String[] topics;
	
	@Bean
	public TopicExchange getExchange() {
		return new TopicExchange(exchange);
	}
	
	@Bean(name = "responseQueue")
	public Queue responseQueue() {
		return new AnonymousQueue();
	}
	
	@Bean
	public Declarables setupRequestBindings(TopicExchange exchange, Queue queue) {
		Collection<Declarable> declarables = new HashSet<>();
		for (String topic : topics)
			declarables.add(BindingBuilder.bind(queue).to(exchange).with("response" + "." + topic));
		return new Declarables(declarables);
	}

}
