package com.openset.server.aspect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import com.openset.server.utils.ResponseReceiver;
import com.openset.server.web.QueryService;

@SpringBootTest
public class TopicAspectTest {
	
	@Autowired
	private TopicAspect topicAspect;

	@Autowired
	private QueryService queryService;

	@MockBean
	private ResponseReceiver receiver;
	
	@MockBean
	private RabbitTemplate template;
	
	@Test
	public void testSupportedTopic() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"example"});
		when(receiver.getAllMessageWithId(anyString())).thenReturn("{\"response\":[]}");
		String actualResponse = queryService.queryDatasources("example", "test");
		assertNotEquals("Topic example isn't supported by this Server", actualResponse);
	}
	
	@Test
	public void testNotSupportedTopic() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"example"});
		String actualResponse = queryService.queryDatasources("test", "test");
		assertEquals("Topic test isn't supported by this Server", actualResponse);
	}
	
	@Test
	public void checkStarTopic() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"*"});
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "anything"));
	}
	
	@Test
	public void checkStarTopic2() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"*", "another", "example"});
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "anything"));
	}
	
	@Test
	public void checkTopicsSearch_withOneTopic() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"example"});
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "example"));
	}
	
	@Test
	public void checkTopicsSearch_insensitive() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"example"});
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "ExAmple"));
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "EXAMPLE"));
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "eXamplE"));
	}
	
	@Test
	public void checkTopicsSearch_insensitive2() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"ExAmPlE"});
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "example"));
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"EXAMPLE"});
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "example"));
	}
	
	@Test
	public void checkTopicsSearch_withThreeTopic() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"example", "prova", "test"});
		assertTrue((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "test"));
	}
	
	@Test
	public void checkNotFoundTopic() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"example", "test"});
		assertFalse((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "prova"));
	}
	
	@Test
	public void checkNotFoundTopic2() {
		ReflectionTestUtils.setField(topicAspect, "topics", new String[] {"example", "test"});
		assertFalse((boolean) ReflectionTestUtils.invokeMethod(topicAspect, "inTopic", "testt"));
	}
}
