package com.openset.server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.openset.server.annotations.SecureJWT;

@RestController
@RequestMapping("/api")
public class ServerController {

	@Autowired
	private QueryService queryService;

	@SecureJWT
	@GetMapping(path = "/query", produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> query(@RequestParam String topic, @RequestParam String argument,
			@RequestHeader(name = "Authorization", required = false) String jwt) {
		return ResponseEntity.status(HttpStatus.OK).body(queryService.queryDatasources(topic, argument));
	}

}
