package com.openset.server.unit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import com.openset.server.web.QueryService;
import com.openset.server.web.ServerController;

@WebMvcTest(ServerController.class)
@TestPropertySource("classpath:application-test.properties")
public class ServerControllerTest {
	
	@Autowired
	private MockMvc mvc;

	@MockBean
	private QueryService service;
	
	@Test
	public void testSearch1() throws Exception {
		String expectedJsonResponse = "{\n" + "  \"response\" : [ [ {\n" + "    \"Abitanti\" : \"27785\",\n"
				+ "    \"CAP\" : \"95014\",\n" + "    \"Regione\" : \"SIC\",\n" + "    \"Prefisso\" : \"095\",\n"
				+ "    \"CodFisco\" : \"E017\",\n" + "    \"Istat\" : \"087017\",\n" + "    \"Provincia\" : \"CT\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/087/017/\",\n" + "    \"Comune\" : \"Giarre\"\n"
				+ "  } ] ]\n" + "}";
		when(service.queryDatasources("comuni", "Giarre")).thenReturn(expectedJsonResponse);

		mvc.perform(get("/api/query?topic=comuni&argument=Giarre").contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer "+getToken()))
				.andExpect(status().isOk()).andExpect(jsonPath("$.response[0][0].Abitanti", is("27785")))
				.andExpect(jsonPath("$.response[0][0].CAP", is("95014")));
	}

	
	@Test
	public void testSearch2() throws Exception {
		String expectedJsonResponse = "{\n" + "  \"response\" : [ [ {\n" + "    \"Abitanti\" : \"3452\",\n"
				+ "    \"CAP\" : \"85050\",\n" + "    \"Regione\" : \"BAS\",\n" + "    \"Prefisso\" : \"0975\",\n"
				+ "    \"CodFisco\" : \"M269\",\n" + "    \"Istat\" : \"076100\",\n" + "    \"Provincia\" : \"PZ\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/076/100/\",\n" + "    \"Comune\" : \"Paterno\"\n"
				+ "  }, {\n" + "    \"Abitanti\" : \"49578\",\n" + "    \"CAP\" : \"95047\",\n"
				+ "    \"Regione\" : \"SIC\",\n" + "    \"Prefisso\" : \"095\",\n" + "    \"CodFisco\" : \"G371\",\n"
				+ "    \"Istat\" : \"087033\",\n" + "    \"Provincia\" : \"CT\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/087/033/\",\n" + "    \"Comune\" : \"Patern�\"\n"
				+ "  }, {\n" + "    \"Abitanti\" : \"1352\",\n" + "    \"CAP\" : \"87040\",\n"
				+ "    \"Regione\" : \"CAL\",\n" + "    \"Prefisso\" : \"0984\",\n" + "    \"CodFisco\" : \"G372\",\n"
				+ "    \"Istat\" : \"078094\",\n" + "    \"Provincia\" : \"CS\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/078/094/\",\n"
				+ "    \"Comune\" : \"Paterno Calabro\"\n" + "  }, {\n" + "    \"Abitanti\" : \"2603\",\n"
				+ "    \"CAP\" : \"83052\",\n" + "    \"Regione\" : \"CAM\",\n" + "    \"Prefisso\" : \"0827\",\n"
				+ "    \"CodFisco\" : \"G370\",\n" + "    \"Istat\" : \"064070\",\n" + "    \"Provincia\" : \"AV\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/064/070/\",\n" + "    \"Comune\" : \"Paternopoli\"\n"
				+ "  } ] ]\n" + "}";
		when(service.queryDatasources("comuni", "Patern")).thenReturn(expectedJsonResponse);

		mvc.perform(get("/api/query?topic=comuni&argument=Patern").contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer "+getToken()))
				.andExpect(status().isOk()).andExpect(jsonPath("$.response[0]", hasSize(4)))
				.andExpect(jsonPath("$.response[0][1].Provincia", is("CT")));
	}

	@Test
	@Disabled
	public void badUsernameAndPassword() throws Exception {
		String expectedJsonResponse = "{\n" + "  \"response\" : [ [ {\n" + "    \"Abitanti\" : \"3452\",\n"
				+ "    \"CAP\" : \"85050\",\n" + "    \"Regione\" : \"BAS\",\n" + "    \"Prefisso\" : \"0975\",\n"
				+ "    \"CodFisco\" : \"M269\",\n" + "    \"Istat\" : \"076100\",\n" + "    \"Provincia\" : \"PZ\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/076/100/\",\n" + "    \"Comune\" : \"Paterno\"\n"
				+ "  }, {\n" + "    \"Abitanti\" : \"49578\",\n" + "    \"CAP\" : \"95047\",\n"
				+ "    \"Regione\" : \"SIC\",\n" + "    \"Prefisso\" : \"095\",\n" + "    \"CodFisco\" : \"G371\",\n"
				+ "    \"Istat\" : \"087033\",\n" + "    \"Provincia\" : \"CT\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/087/033/\",\n" + "    \"Comune\" : \"Patern�\"\n"
				+ "  }, {\n" + "    \"Abitanti\" : \"1352\",\n" + "    \"CAP\" : \"87040\",\n"
				+ "    \"Regione\" : \"CAL\",\n" + "    \"Prefisso\" : \"0984\",\n" + "    \"CodFisco\" : \"G372\",\n"
				+ "    \"Istat\" : \"078094\",\n" + "    \"Provincia\" : \"CS\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/078/094/\",\n"
				+ "    \"Comune\" : \"Paterno Calabro\"\n" + "  }, {\n" + "    \"Abitanti\" : \"2603\",\n"
				+ "    \"CAP\" : \"83052\",\n" + "    \"Regione\" : \"CAM\",\n" + "    \"Prefisso\" : \"0827\",\n"
				+ "    \"CodFisco\" : \"G370\",\n" + "    \"Istat\" : \"064070\",\n" + "    \"Provincia\" : \"AV\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/064/070/\",\n" + "    \"Comune\" : \"Paternopoli\"\n"
				+ "  } ] ]\n" + "}";
		when(service.queryDatasources("comuni", "Patern")).thenReturn(expectedJsonResponse);

		mvc.perform(get("/api/query?topic=comuni&argument=Patern").contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer "+"wrong token"))
				.andExpect(status().is(500));
	}

	@Test
	public void testSearchWithNullResult() throws Exception {
		String expectedJsonResponse = "{" + "  \"response\" : [ ]" + "}";
		when(service.queryDatasources(eq("comuni"), anyString())).thenReturn(expectedJsonResponse);

		mvc.perform(get("/api/query?topic=comuni&argument=Bugliano").contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer "+getToken()))
				.andExpect(status().isOk()).andExpect(jsonPath("$.response", hasSize(0)));
	}
	
	private String getToken() {
		HttpRequest request = HttpRequest
				.newBuilder()
				.uri(URI.create("https://openset-auth0.herokuapp.com/authentication"))
				.header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(emailAndPassword()))
				.build();
		HttpClient httpClient = HttpClient.newBuilder()
	            .version(HttpClient.Version.HTTP_1_1)
	            .build();
		HttpResponse<String> response = null;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		String jwt = response.body().replace("{","").replace("\"jwt\":\"","").replace("\"", "").replace("\n", "")
				.replace("}","").replace(" ","");
		return jwt;
	}
	private String emailAndPassword() {
		return "{\r\n" + 
				"  \"email\": \"admin@admin.com\",\r\n" + 
				"  \"password\": \"admin\"\r\n" + 
				"}";
	}
}