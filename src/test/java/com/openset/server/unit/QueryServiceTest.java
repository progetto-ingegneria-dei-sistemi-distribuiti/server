package com.openset.server.unit;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.TestPropertySource;

import com.openset.server.utils.ResponseReceiver;
import com.openset.server.web.QueryService;
import com.rabbitmq.client.Channel;

@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class QueryServiceTest {
	
	@MockBean
	private ResponseReceiver receiver;

	@Autowired
	private QueryService service;
	
	@Autowired
	private Config config;

	@Test
	public void testSearch1() throws JSONException {
		String expectedJsonResponse = "{\n" + "  \"response\" : [ [ {\n" + "    \"Abitanti\" : \"27785\",\n"
				+ "    \"CAP\" : \"95014\",\n" + "    \"Regione\" : \"SIC\",\n" + "    \"Prefisso\" : \"095\",\n"
				+ "    \"CodFisco\" : \"E017\",\n" + "    \"Istat\" : \"087017\",\n" + "    \"Provincia\" : \"CT\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/087/017/\",\n" + "    \"Comune\" : \"Giarre\"\n"
				+ "  } ] ]\n" + "}";
		when(receiver.getAllMessageWithId(anyString())).thenReturn(expectedJsonResponse);

		String actualResponseJson = service.queryDatasources("comuni", "Giarre");

		JSONAssert.assertEquals(expectedJsonResponse, actualResponseJson, JSONCompareMode.LENIENT);
	}

	@Test
	public void testSearch2() throws JSONException {
		String expectedJsonResponse = "{\n" + "  \"response\" : [ [ {\n" + "    \"Abitanti\" : \"3452\",\n"
				+ "    \"CAP\" : \"85050\",\n" + "    \"Regione\" : \"BAS\",\n" + "    \"Prefisso\" : \"0975\",\n"
				+ "    \"CodFisco\" : \"M269\",\n" + "    \"Istat\" : \"076100\",\n" + "    \"Provincia\" : \"PZ\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/076/100/\",\n" + "    \"Comune\" : \"Paterno\"\n"
				+ "  }, {\n" + "    \"Abitanti\" : \"49578\",\n" + "    \"CAP\" : \"95047\",\n"
				+ "    \"Regione\" : \"SIC\",\n" + "    \"Prefisso\" : \"095\",\n" + "    \"CodFisco\" : \"G371\",\n"
				+ "    \"Istat\" : \"087033\",\n" + "    \"Provincia\" : \"CT\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/087/033/\",\n" + "    \"Comune\" : \"Patern�\"\n"
				+ "  }, {\n" + "    \"Abitanti\" : \"1352\",\n" + "    \"CAP\" : \"87040\",\n"
				+ "    \"Regione\" : \"CAL\",\n" + "    \"Prefisso\" : \"0984\",\n" + "    \"CodFisco\" : \"G372\",\n"
				+ "    \"Istat\" : \"078094\",\n" + "    \"Provincia\" : \"CS\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/078/094/\",\n"
				+ "    \"Comune\" : \"Paterno Calabro\"\n" + "  }, {\n" + "    \"Abitanti\" : \"2603\",\n"
				+ "    \"CAP\" : \"83052\",\n" + "    \"Regione\" : \"CAM\",\n" + "    \"Prefisso\" : \"0827\",\n"
				+ "    \"CodFisco\" : \"G370\",\n" + "    \"Istat\" : \"064070\",\n" + "    \"Provincia\" : \"AV\",\n"
				+ "    \"Link\" : \"http://www.comuni-italiani.it/064/070/\",\n" + "    \"Comune\" : \"Paternopoli\"\n"
				+ "  } ] ]\n" + "}";
		when(receiver.getAllMessageWithId(anyString())).thenReturn(expectedJsonResponse);

		String actualResponseJson = service.queryDatasources("comuni", "Patern");

		JSONAssert.assertEquals(expectedJsonResponse, actualResponseJson, JSONCompareMode.LENIENT);
	}

	@Test
	public void testSearch_withEmptyResult() throws JSONException {
		String expectedJsonResponse = "{\n" + "  \"response\" : [ ]\n" + "}";
		when(receiver.getAllMessageWithId(anyString())).thenReturn(expectedJsonResponse);
		String actualResponseJson = service.queryDatasources("comuni", "Bugliano");
		JSONAssert.assertEquals(expectedJsonResponse, actualResponseJson, JSONCompareMode.LENIENT);
	}
	
	@Test
	public void testCorrectReceiveOfSentMessage() {
		service.queryDatasources("comuni", "Catania");
		assertEquals("Catania", this.config.body);
		assertNotNull(this.config.correlationId);
	}
	
	@TestConfiguration
	@EnableRabbit
	public static class Config {

		protected String body;
		protected String correlationId;
		
		@Bean
		@Primary
		public RabbitTemplate template() {
			return new TestRabbitTemplate(connectionFactory());
		}

		@Bean
		public ConnectionFactory connectionFactory() {
			ConnectionFactory factory = mock(ConnectionFactory.class);
			Connection connection = mock(Connection.class);
			Channel channel = mock(Channel.class);
			willReturn(connection).given(factory).createConnection();
			willReturn(channel).given(connection).createChannel(anyBoolean());
			given(channel.isOpen()).willReturn(true);
			return factory;
		}

		@Bean
		public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
			SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
			factory.setConnectionFactory(connectionFactory());
			return factory;
		}

		@RabbitListener(queues = "request.comuni")
		public void foo(Message in) {
			this.body = new String(in.getBody());
			this.correlationId = in.getMessageProperties().getCorrelationId();
		}

	}
	
}
