package com.openset.server.web;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openset.server.utils.ResponseReceiver;

@Service
public class QueryService {

	private RabbitTemplate template;
	private ResponseReceiver receiver;
	
	private final long WAIT_TIME = 3;
	
	@Autowired
	public QueryService(RabbitTemplate template, ResponseReceiver receiver, Exchange exchange) {
		template.setExchange(exchange.getName());
		this.template = template;
		this.receiver = receiver;
	}
	
	public String queryDatasources(String topics, String argument) {
		String correlationId = UUID.randomUUID().toString();
		List<String> routingKeys = List.of(topics.split("\\|")).stream()
				.map(topic -> "request" + "." + topic).collect(Collectors.toList());
		routingKeys.stream()
			.forEach(routingKey -> template.convertAndSend(routingKey, argument.getBytes(), message -> postProcessMessage(message, correlationId)));
		waitFor(WAIT_TIME, TimeUnit.SECONDS);
		return receiver.getAllMessageWithId(correlationId);
	}
	
	private void waitFor(long time, TimeUnit timeUnit) {
		try {
			timeUnit.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private Message postProcessMessage(Message message, String correlationId) throws AmqpException {
		message.getMessageProperties().setCorrelationId(correlationId);
		return message;
	}

}
