package com.openset.server.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.openset.server.exceptions.JsonParsingException;
import com.openset.server.json.JsonArray;
import com.openset.server.json.JsonObject;

@Component
public class ResponseReceiver {

	private Map<String, List<Message>> correlatedMessageMap;
	
	@PostConstruct
	private void setup() {
		correlatedMessageMap = new HashMap<>();
	}
	
	@RabbitListener(queues = "#{responseQueue.name}")
	public void messageListener(Message message) {
		String correlationId = message.getMessageProperties().getCorrelationId();
		if(correlatedMessageMap.containsKey(correlationId))
			addReceivedMessageToList(message, correlationId);
		else 
			createMessageListWithReceivedMessageAndPutItToMap(message, correlationId);
	}
	
	private void addReceivedMessageToList(Message message, String correlationId) {
		List<Message> list = correlatedMessageMap.get(correlationId);
		list.add(message);
	}

	private void createMessageListWithReceivedMessageAndPutItToMap(Message message, String correlationId) {
		List<Message> messageList = new ArrayList<>();
		messageList.add(message);
		correlatedMessageMap.put(correlationId, messageList);
	}
	
	public String getAllMessageWithId(String correlationId){
		JsonObject rootNode = new JsonObject();
		JsonArray responseArray = new JsonArray();
		if(correlatedMessageMap.containsKey(correlationId))
			correlatedMessageMap.remove(correlationId).forEach(message -> addResponseMessage(message, responseArray));
		rootNode.put("response", responseArray);
		return rootNode.toString();
	}
	
	private void addResponseMessage(Message message, JsonArray responseArray) {
		try {
			JsonArray responseJson = new JsonArray(new String(message.getBody()));
			responseArray.put(responseJson);
		} catch (JsonParsingException e) {
			//invalid response
			e.printStackTrace();
		}
	}

}
